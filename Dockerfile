# Define a imagem base a ser utilizada pelo docker
FROM node:14

# Define o diretório de trabalho dentro do container
WORKDIR /usr/src/app

# Copia o package.json e o package-lock.json para o diretório de trabalho
COPY package*.json ./

# Instala as dependências do projeto, nesse caso utilizei o npm
RUN npm install 

# Copia o restante do código para o diretório de trabalho do container
COPY . .

# Expõe a porta em que a aplicação irá rodar
EXPOSE 3000

# Inicia a aplicação
CMD [ "npm", "start" ]
