const express = require("express")
var cookieParser = require('cookie-parser')
var session = require('express-session')

const app = express()

app.listen(3001)

const oneDay = 1000 * 60 * 60 * 24;
app.use(session({
  secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
  saveUninitialized: true,
  cookie: { maxAge: oneDay },
  resave: false
}));

app.get("/cookie", (req, resp) => {
  if (req.session.count) {
    req.session.count++
  } else {
    req.session.count = 1
  }
  resp.send(req.session.count + "")
})
