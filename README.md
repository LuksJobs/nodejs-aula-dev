# Rode seu aplicativo Node.js em container usando o Docker


Aplicação básica em NodeJS: "Hello World" e a funcionalidade do Cookie Parser, usando as extensões: express, express-session e cookie-parser.

Para rodar, entrar no diretório do projeto e executar: 

```
"npm run dev" ou "node nomedoarquivo.js" 
```
Depois de buildar a aplicação, basta agora só acessar pelo navegador de internet através da porta definida: "localhost:3000". 

```
cookie.js  node_modules  package-lock.json  test.js
index.js   package.json  README.md
```
## Buildando' nossa imagem Docker

```
docker build -t nome-da-imagem .
```

Este comando irá construir a imagem Docker a partir do Dockerfile e dará um nome à imagem (nome-da-imagem). O ponto final (.) indica que o Dockerfile está na raiz do diretório atual.

Rodando nosso container a partir da imagem criada

```
docker run -p 3000:3000 nome-da-imagem
```

Este comando irá rodar o container Docker a partir da imagem criada anteriormente (nome-da-imagem) e mapeará a porta da máquina host (3000) para a porta do container Docker (3000).

Com esses passos básicos, você deve ser capaz de rodar sua aplicação Node.js em um container Docker. É importante lembrar que você pode precisar adaptar o Dockerfile dependendo das necessidades específicas do seu projeto.

## Acessando sua aplicação via Web 

Para acessar seu container, bata digitar em seu navegador de internete preferido: "localhost:3000" ou então especificar seu IP e Porta: "10.10.1.0:3000". 

## Parando ou Removendo o Container

Já terminou os testes e agora não há mais a necessidade de manter o container em "pé" ou "rodando", para parar ou remover esse container, existe os dois comandos abaixo:

docker container stop "nome-do-container" ou "id-do-container" #parar a execução do container

docker container rm -f "nome-do-container" ou "id-do-container" #remover o container